<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\CanResetPassword;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Many-Many Relationship to Courses
     */

    public function courses()
    {
        return $this->hasMany('App\Course');
    }

    /**
     * Many-Many Relationship to Roles
     */

    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

    /**
     * Check to see if user has the role being checked
     */

    public function hasRole($role)
    {
        return $this->roles->contains('name', $role);
    }


   /**
     * Many-Many Relationship to Projects
     */

    public function projects()
    {
        return $this->hasMany('App\Project');
    }

    /**
     * Create an array of users for a select input
     */

    public static function selectArray()
    {
        $users = User::get();
        $array = [];

        foreach ($users as $user) {
            $array[$user->id] = $user->first_name . ' ' . $user->last_name;
        }

        return $array;
    }

}
