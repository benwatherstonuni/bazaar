<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Course;
use App\Project;
use App\Http\Requests;
use App\Http\Controllers\Users;
use App\Http\Controllers\Controller;


class UsersController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
    	$users = User::orderBy('last_name', 'asc')->get();


    	return view('pages.users', compact('users'));
    }

    public function show($id)
    {
        $users = User::findOrFail($id);

        $courses = Course::where('user_id', '=', $id)->get();

        $projects = Project::where('leader_id', '=', $id)->get();



    	return view('pages.users.show', compact('users', 'courses', 'projects'));

    }


public function edit($id)
    {
        $user = User::findOrFail($id);

        $roles = Role::get();

        return view('pages.users.edit', compact('user', 'roles'));
    }



    public function create()
    {
         /*
        * Returns the blade pages/courses/create.blade.php where new courses will be created
        */
        $roles = Role::get();

        return view('pages.users.create', compact('roles'));
    }


    public function store(Requests\UsersRequest $request)
    {

        $user = User::create($request->all());

        $user->roles()->attach($request->get('role'));

        \Session::flash('flash_message', '<div class="flash flash-add">User <a href="' . url('/users/' . $user->id ) .'">' . $user->first_name . ' ' . $user->last_name . '</a> CREATED!<span>x</span></div>');

        return redirect('users');
    }



    public function update($id, Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users,email,' . $id
        ]);

        $user = User::findOrFail($id);
        $user->update($request->all());


        \Session::flash('flash_message', '<div class="flash flash-update">User <a href="' . url('/users/' . $user->id ) . '">' . $user->first_name . ' ' . $user->last_name . '</a> UPDATED!<span>x</span></div>');

        /**
         * If the request has the role input, change the user's role.
         */
        if ($request->has('role'))
        {
            // Remove current role from the user.
            $user->roles()->detach();

            // Add the new role to the table
            $user->roles()->attach($request->get('role'));
        }

        return back();
    }


    public function destroy($id)
    {

        // Added to confirm the delete of items
        $user = User::findORFail($id);
        $user->delete();

        \Session::flash('flash_message', '<div class="flash flash-delete">User ' . $user->first_name . ' ' . $user->last_name . ' DELETED!<span>x</span></div>');

        return redirect('users');
    }

    public function changePassword($id)
    {
        $user = User::findOrFail($id);

        return view('pages.users.edit.password', compact('user'));
    }


    public function updatePassword($id, Request $request)
    {

        $this->validate($request, [
            'password' => 'required|confirmed|min:6'
        ]);

        $user = User::findOrFail($id);
        $user->update(['password' => bcrypt($request->get('password'))]);

        $roles = Role::get();

         \Session::flash('flash_message', '<div class="flash flash-update">Password UPDATED!<span>x</span></div>');

        return view('pages.users.edit', compact('user', 'roles'));

    }


}
