<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Course;
use App\Project;
use App\User;
use App\Role;
use Carbon\Carbon;

class WelcomeController extends Controller
{
    public function index()
    {
    	/*
    	 * Gets current time
    	 * Takes the hour in 24 hour format
    	 */
    	$time = Carbon::now('Europe/London');

    	$hours = $time->format('H');

    	$courses = Course::get();
	    
        $projects = Project::get();

    	return view('pages.welcome', compact('hours', 'courses', 'projects'));

    }
}
