<?php

namespace App\Http\Controllers;

use App\Course;
use App\Project;
use App\User;
use App\Http\Requests;
use App\Http\Requests\CourseRequest;
use Illuminate\Http\Request;
use Illuminate\HttpResponse;
use App\Http\Controllers\Controller;

class CoursesController extends Controller
{
	public function index(){

		/*
		* Retrives all Courses and Projects
		*/

	    $courses = Course::orderBy('name', 'asc')->get();
	    $projects = Project::get();

	    $users = User::get();

	    /*
	    * Returns the blade pages/courses.blade.php
	    */

    	return view('pages.courses', compact('courses', 'projects', 'users'));

	}


	public function show($id){

	    /**
	     * Gets the course where the id field = $id OR 
	     * shows a 404 error page.
	     *
	     * Gets the projects where courses ID is found so feedback can be given to the user
	     */

	    $course = Course::findOrFail($id);
	    $projects = Project::orderBy('id', 'desc')->where('course_id', $id)->get();

	     $users = User::get();
		
	    /*
	    * Returns the blade pages/courses/show.blade.php where individual courses will be shown
	    */

    	return view('pages.courses.show', compact('course', 'projects', 'users'));

	}


	/*
	* Finds the current course ID
	* Redirects to pages/courses/edit.blade.php
	*/

	public function edit($id)
	{
		$course = Course::findOrFail($id);

		return view('pages.courses.edit', compact('course'));
	}

	

	public function create()
	{
		/*
	     * Returns the blade pages/courses/create.blade.php where new courses will be created
	     */

		return view('pages.courses.create');
	}


	/*
	* Gets all information submitted by the form on pages/courses/create.blade.php 
	* Uses submitted information to create a new course
	* Redirects the current url back to pages/courses.blade.php
	*
	* @param CreateCourseRequest $request
	* @return Response
	*/

	public function store(CourseRequest $request)
	{

		// Course::create($request->all());

		$course = auth()->user()->courses()->create($request->all());

		\Session::flash('flash_message', '<div class="flash flash-add">Course <a href="' . url('/courses/' . $course->id) .'">' . $course->name . '</a> CREATED!<span>x</span></div>');

		return redirect('courses');
	}



	/*
	* Finds the current course ID
	* Updates the current record
	*/

	public function update($id, CourseRequest $request)
	{
		$course = Course::findOrFail($id);

		$course->update($request->all());

		\Session::flash('flash_message', '<div class="flash flash-update">Course <a href="' . url('/courses/' . $course->id) . '">' . $course->name . '</a> UPDATED!<span>x</span></div>');

		return back();
	}


	public function destroy($id)
    {

        // Added to confirm the delete of items
        $course = Course::findORFail($id);
        $course->delete();

       	\Session::flash('flash_message', '<div class="flash flash-delete">Course ' . $course->name . ' DELETED!<span>x</span></div>');

        return redirect('courses');
    }


}
