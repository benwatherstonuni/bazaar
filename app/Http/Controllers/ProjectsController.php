<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Project;
use App\Course;
use App\User;
use App\Role;
use App\Http\Requests;
use App\Http\Requests\ProjectRequest;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Projects;
use App\Http\Controllers\Courses;
use Carbon\Carbon;

class ProjectsController extends Controller
{
    	public function index(){

		/*
		* Retrives all Courses and Projects
		*/

	    $projects = Project::orderBy('id', 'desc')->take(5)->get();

	    $users = User::get();
	
	    /*
	    * Returns the blade pages/courses.blade.php
	    */

    	return view('pages.projects', compact('projects', 'users'));

	}

	/**
	 * @param int $id -> project ID
	 */
	public function show($id){

	    $project = Project::with('course')->findOrFail($id);

	    $courseLeader = User::find($project->course->user_id);

	    $projectLeader = User::find($project->leader_id);


	    $users = User::get();
	

	    /*
	    * Returns the blade pages/courses/show.blade.php where individual courses will be shown
	    */

    	return view('pages.projects.show', compact('project', 'courseLeader', 'projectLeader', 'users'));

	}



	public function edit($id)
	{
		$project = Project::findOrFail($id);

		return view('pages.projects.edit', compact('project'));
	}



	public function create()
	{
		 /*
	    * Returns the blade pages/courses/create.blade.php where new courses will be created
	    */

		return view('pages.projects.create');
	}


	/**
	 * Creates project and assigns current users ID
	 */

	public function store(ProjectRequest $request)
	{

		$project = Project::create($request->all());

		\Session::flash('flash_message', '<div class="flash flash-add">Project <a href="' . url('/projects/' . $project->id) . '">' . $project->title . '</a> CREATED!<span>x</span></div>');


		return redirect('projects');
	}



	public function update($id, ProjectRequest $request)
	{
		$project = Project::findOrFail($id);

		$project->update($request->all());

		\Session::flash('flash_message', '<div class="flash flash-update"><p>Project <a href="' . url('/projects/' . $project->id) .'">' . $project->title . '</a> UPDATED!</p><span>x</span></div>');

		return back();
	}


	public function destroy($id)
    {

        // Added to confirm the delete of items
        $project = Project::findORFail($id);
        $project->delete();

        \Session::flash('flash_message', '<div class="flash flash-delete">Project ' . $project->title . ' DELETED!<span>x</span></div>');

        return redirect('projects');
    }

}
