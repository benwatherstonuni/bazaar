<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CourseRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * Name field must have a minimum of 5 characters
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:5'
        ];
    }
}
