<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    
	/**
	 * Routes for Login
	 */

	Route::get('/', 'Auth\AuthController@getLogin');
	Route::get('/login', 'Auth\AuthController@getLogin');
	Route::post('/login', 'Auth\AuthController@postLogin');

	/**
	 * Logout
	 */


	Route::get('/logout', 'Auth\AuthController@logout');

	/*
	* Routes for Register
	*/

	Route::get('/logout', 'Auth\AuthController@logout');

	/**
	 * Routes for Register
	 */
	Route::get('/register', 'Auth\AuthController@getRegister');
	Route::post('/register', 'Auth\AuthController@postRegister');


	/*
	 * Password reset request
	 */

	Route::get('/password', 'Auth\PasswordController@index');
	
	Route::get('password/email', 'Auth\PasswordController@getEmail');
	Route::post('password/email', 'Auth\PasswordController@postEmail');

	/*
	 * Password reset
	 */

	Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
	Route::post('password/reset', 'Auth\PasswordController@postReset');

});


Route::group(['middleware' => ['web', 'auth']], function () {

	/**
	 * Route for Welcome page
	 */
	Route::get('/welcome', 'WelcomeController@index');


	/**
	 * Routes for Projects 
	 */
	Route::resource('projects', 'ProjectsController');

	/**
	 * Routes for Courses
	 */
	Route::resource('courses', 'CoursesController');
	
 
	/**
	 * Routes for Users
	 */
	Route::get('users/{id}/edit/password', 'UsersController@changePassword');
	Route::patch('users/{id}/edit/password', 'UsersController@updatePassword');

	Route::resource('users', 'UsersController');
	
	
	
 
 });
