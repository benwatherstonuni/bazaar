<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Project extends Model
{
    
	protected $table = 'projects';
	protected $fillable = ['title', 'content', 'leader_id', 'course_id'];

	/**
	 * Set up the relationship with course
	 */
	public function course()
	{
		return $this->belongsTo('App\Course');
	}

}
