<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function users()
    {
    	return $this->belongsToMany('App\User');
    }

    public function permissions()
    {
    	return $this->belongsToMany('App\Permission');
    }

    public static function usersWithRole($role)
    {
    	// Get the given role name and getting the record from the DB
  		$role = Role::where('name', $role)->with('users')->first();

  		// Create empty array to store the IDs as keys
  		$roleUsers = [];

  		// Loop through the users with the role and 
  		// add them to the $roleUsers array
  		foreach ($role->users as $user)
  		{
  			$roleUsers[$user->id] = $user->first_name . ' ' . $user->last_name;
  		}

  		// Return the array.
  		return $roleUsers;
    }
}
