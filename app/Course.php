<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Course extends Model
{

	/*
	* Protects the input fields from mass assignment.
	*/
    protected $fillable = [
    	'name',
        'content',
    	'user_id'
    ];

    /**
     * Define the relationship to projects
     */
    public function projects()
    {
    	return $this->hasMany('App\Project');
    }


    public function courseList($courses)
    {
        $courses = Course::orderBy('name', 'asc')->get();

        foreach ($courses as $course) {
            $course->name;
        }
    }

    public static function selectCourse()
    {
        $courses = Course::orderBy('name', 'asc')->get();
        $courseArray = [];

        foreach ($courses as $course) {
            $courseArray[$course->id] = $course->name;
        }

        return $courseArray;
    }

    
}
