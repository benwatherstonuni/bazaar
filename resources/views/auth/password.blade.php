@extends('layouts.login')


@section('title', 'Password Reset')

@section('h1', '- Password Reset')

@section('content')


{!! Form::open(['url'=> '/password/email']) !!}

<div class="form-group">

@include('errors.form')

	{!! Form::label('email', 'Email') !!}

	{!! Form::email('email', old('email'), ['id' => 'email', 'placeholder'=>'Email']) !!}


	{!! Form::submit('Send Password Reset Link', ['id' => 'submit']) !!}
</div>
{!! Form::close() !!}
<div class="form-forgot">
	<a href="{{ url('/register') }}">Register Account</a><span> | </span><a href="{{ url('/login') }}">Already Registered?</a>
</div>

@endsection