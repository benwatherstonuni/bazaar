@extends('layouts.login')


@section('title', 'Login')

@section('h1', '- Login')

@section('content')


{!! Form::open(['url'=> 'login']) !!}

<div class="form-group">

@include('errors.form')

	{!! Form::label('email', 'Email') !!}

	{!! Form::email('email', old('email'), ['id' => 'email', 'placeholder'=>'Email']) !!}

	{!! Form::label('password', 'Password') !!}

	{!! Form::password('password', ['id' => 'password', 'placeholder'=>'Password']) !!}

	{!! Form::submit('Login', ['id' => 'submit']) !!}
</div>
{!! Form::close() !!}
<div class="form-forgot">
	<a href="{{ url('/register') }}">Register Account</a><span> | </span><a href="{{ url('/password') }}">Forgotten Password?</a>
</div>

@endsection