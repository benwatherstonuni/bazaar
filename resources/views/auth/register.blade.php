@extends('layouts.login')


@section('title', 'Register')

@section('h1', '- Register')

@section('content')

<div class="form-group">

{!! Form::open(['url'=> 'register']) !!}

@include('pages.users._form', ['buttonText' => 'Register Account'])


{!! Form::close() !!}
</div>
<div class="form-forgot">
	<a href="{{ url('/login') }}">Already Registered?</a><span> | </span><a href="{{ url('/password') }}">Forgotten Password?</a>
</div>

@endsection