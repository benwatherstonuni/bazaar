@extends('layouts.master')


@section('title', 'Welcome')



@section('content')

{{-- Dynamic greeting depending on time of day --}}

@if($hours < 12)
	<h1 class="morning">Good Morning, {{ auth()->user()->first_name . '!'}}</h1>
@elseif(($hours > 11) && ($hours < 17))
	<h1 class="afternoon">Good Afternoon, {{ auth()->user()->first_name . '!'}}</h1>
@else
	<h1 class="evening">Good Evening, {{ auth()->user()->first_name . '!'}}</h1>

@endif

<div class="wrap">
	<p>Welcome to Project Bazaar, a system designed to help Computing Students choose a final year project.</p>
</div>






<div class="dashboard">

	<div class="half">
		<span>{{ count($courses) }}</span>
		<h2>Courses Avaiable</h2>
		<a class="btn" href="{{ url('/courses/') }}">View All Courses</a>
	</div>

	<div class="half">
		<span>{{ count($projects) }}</span>
		<h2>Projects Avaiable</h2>
		<a class="btn" href="{{ url('/projects/') }}">View Most Recent</a>
	</div>

</div>

@endsection