@extends('layouts.master')


@section('title', 'Projects')



@section('content')

<h1>Most Recent Projects</h1>

@foreach(array_slice($users->toArray(), 0, 1) as $user)

	@if( auth()->user()->roles->first()->name === "student")

	@else

		<div class="row">
			<a class="create" href="{{ url('/projects/create') }}">Create Project</a>
		</div>

	@endif


@endforeach



@foreach($projects as $project)

	<article class="post">

		<a class="btn" href="{{ url('/projects', $project->id) }}">{{ str_limit($project->title, $limit = 30, $end = '...')  }}</a>
		
		<span>{!! str_limit($project->content, $limit = 200, $end = '...') !!}</span>
	</article>

@endforeach

@stop