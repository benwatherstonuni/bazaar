@extends('layouts.master')


@section('title', 'Courses')



@section('content')

<h1>Courses ( {{ count($courses) }} )</h1>


@foreach(array_slice($users->toArray(), 0, 1) as $user)

	@if( auth()->user()->roles->first()->name === "student")

	@else
		<div class="row">
			<a class="create" href="{{ url('/courses/create') }}">Create Course</a>
		</div>
	@endif

@endforeach


<ul class="list">
	@foreach($courses as $course)
		<li><a href="{{ url('/courses', $course->id) }}">{{ $course->name }}</a></li>
	@endforeach
</ul>

@stop