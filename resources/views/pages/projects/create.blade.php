@extends('layouts.master')

@section('title', 'Create Project')

@section('content')
<h1>Create Project</h1>


<!-- Form sends user to the courses page -->
<div class="form-group">
	

{!! Form::open(['url' => 'projects']) !!}

	@include('pages.projects._form', ['buttonText' => 'Create Project'])

{!! Form::close() !!}

</div>
<div class="row">
	<a class="btn back" href="{{ url('/projects') }}">Back to Projects</a>
</div>

@stop