@extends('layouts.master')

@section('title', 'Edit - ' . $project->title)


				
@section('content')
<h1>Edit - {{ $project->title }}</h1>


<!-- Form sends user to the courses page -->
<div class="form-group">
	

{!! Form::model($project, ['method' => 'PATCH', 'action' => ['ProjectsController@update', $project->id]]) !!}

	@include('pages.projects._form', ['buttonText' => 'Update Project'])

{!! Form::close() !!}

</div>

<div class="row">	

	<a class="btn back" href="{{ url('/projects/' . $project->id) }}">Back to Projects</a>

</div>

@stop