@include('errors.form')

	{!! Form::label('title', 'Project Name') !!}

	{!! Form::text('title', old('title'), ['id' => 'title', 'placeholder'=>'Project Name']) !!}

	{!! Form::label('leader_id', 'Project Leader') !!}

	{!! Form::select('leader_id', \App\Role::usersWithRole('lecturer'), old('leader_id'), ['id' => 'leader_id']) !!}

	{!! Form::label('course_id', 'Course') !!}

	{!! Form::select('course_id', \App\Course::selectCourse(), old('course_id'), ['id' => 'course_id']) !!}

	{!! Form::label('content', 'Project Content') !!}

	{!! Form::textarea('content', old('content'), ['id' => 'content', 'placeholder'=>'Content']) !!}


	<script>
		CKEDITOR.replace( 'content' );
	</script>

	{!! Form::submit( $buttonText, ['class' => 'btn'] ) !!}