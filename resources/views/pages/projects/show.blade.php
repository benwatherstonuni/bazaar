@extends('layouts.master')

@section('title', $project->title)

@section('content')

<h1>{{ $project->title }}</h1>


@foreach(array_slice($users->toArray(), 0, 1) as $user)

	@if( auth()->user()->roles->first()->name === "student")

	@else

		<div class="row">
			{!! Form::open(['url' => 'projects/' . $project->id, 'method' => 'DELETE']) !!}
				<button type="submit" class="btn delete">Delete</button>
			{!! Form::close() !!}

			<a class="edit btn" href="{{ url('/projects/'. $project->id .'/edit') }}">Edit - {{ str_limit($project->title, $limit = 15, $end = '...') }}</a>
		</div>

	@endif


@endforeach



<main>

<aside>

	<h2>Details</h2>
	
	@if( count($project->updated_at) )
		<h3>Last Updated - {{ $project->updated_at->format('dS F Y') }}</h3>
	@endif

	<a class="btn" href="{{ url('/users/' . $project->leader_id) }}">{{ "Project Leader - " . $projectLeader->first_name . ' ' . $projectLeader->last_name}}</a>

	<a class="btn" href="{{ url('/courses/' . $project->course->id) }}">{{ "Course - " . $project->course->name }}</a>

	<a class="btn" href="{{ url('/users/' . $courseLeader->id) }}"> {{ "Course Leader - " . $courseLeader->first_name . ' ' . $courseLeader->last_name }}</a>

</aside>
	<div class="wrap">
		{!! $project->content !!}
	</div>
</main>



@stop