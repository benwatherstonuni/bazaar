@extends('layouts.master')


@section('title', 'Users')



@section('content')

<h1>Users ( {{ count($users) }} )</h1>


@foreach(array_slice($users->toArray(), 0, 1) as $user)

	@if( auth()->user()->roles->first()->name === "student")

	@else
		<div class="row">

			<a class="create" href="{{ url('/users/create') }}">Create User</a>
			<a class="profile" href="{{ url('/users', auth()->user()->id) }}">My Profile</a>

		</div>
	@endif

@endforeach


<table>
<thead>
	<tr>
		<th>Name</th>
		<th>Email</th>
		<th>Role</th>
		<th>View</th>
	</tr>
</thead>
<tbody>

	@foreach($users as $user)
		<tr>
			<td data-label="Name">{{ $user->first_name . " " . $user->last_name }}</td>
			<td data-label="Email">{{ $user->email }}</td>
			<td data-label="Role">{{ ucfirst($user->roles->first()->name) }}</td>
			<td data-label="View" class="view-cell"><a class="view" href="{{ url('/users', $user->id) }}">View</a></td>
		</tr>
	@endforeach

</tbody>
</table>






@stop