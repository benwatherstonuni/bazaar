@extends('layouts.master')

@section('title', $users->first_name . ' ' . $users->last_name . ' - Profile ')

@section('content')



<h1>User Profile</h1>

@foreach(array_slice($users->toArray(), 0, 1) as $user)

	@if( auth()->user()->id === $users->id | auth()->user()->roles->first()->name === "admin" )

		<div class="row">
			{!! Form::open(['url' => '/users/' . $users->id, 'method' => 'DELETE']) !!}
				<button type="submit" class="btn delete">Delete</button>
			{!! Form::close() !!}

			<a class="edit" href="{{ url('/users/'. $users->id .'/edit') }}">Edit - {{$users->first_name . " " . $users->last_name }}</a>
		</div>
		
	@endif

@endforeach

<div class="user-container">

	<p class="user">{{ $users->first_name . ' ' . $users->last_name }}</p>
	<p>Email - {{ $users->email }}</p>

	@if( $users->roles->first()->name != "student" )
	
		@if(count($courses))
			<div class="half">
				<h3>Course(s)</h2>
				
				<ul>
					@foreach($courses as $course)
						<li>- <a href="{{url('/courses/' . $course->id)}}">{{ $course->name }}</a></li>	
					@endforeach
				</ul>
			</div>
		@endif

		@if(count($projects))
			<div class="half">
				<h3>Project(s)</h2>
				
				<ul>
					@foreach($projects as $project)
						<li>- <a href="{{url('/projects/' . $project->id)}}">{{ $project->title }}</a></li>	
					@endforeach
				</ul>
			</div>
		@endif

	@endif


	
<div class="row">
	<a class="btn email" href="mailto:{{ $users->email }}">Email - {{ $users->first_name . ' ' . $users->last_name }}</a>
</div>
</div>


<div class="row">	

	<a class="btn back" href="{{ url('/users') }}">Back to Users</a>

</div>

@stop