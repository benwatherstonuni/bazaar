@include('errors.form')

	{!! Form::label('first_name', 'First Name') !!}
	
	{!! Form::text('first_name', old('first_name'), ['id' => 'first_name', 'placeholder'=>'First Name']) !!}
	
	{!! Form::label('last_name', 'Last Name') !!}

	{!! Form::text('last_name', old('last_name'), ['id' => 'last_name', 'placeholder'=>'Last Name']) !!}

	{!! Form::label('email', 'Email') !!}

	{!! Form::email('email', old('email'), ['id' => 'email', 'placeholder'=>'Email']) !!}

	
	
	@if (!auth()->check())
	<!-- If user is logged in hide password update - moved to new page -->
		{!! Form::label('password', 'Password') !!}

		{!! Form::password('password', ['id' => 'password', 'placeholder'=>'Password']) !!}

		{!! Form::label('password_confirmation', 'Confirm Password') !!}

		{!! Form::password('password_confirmation', ['id' => 'password_confirmation', 'placeholder'=>'Confirm Password']) !!}		
	@else
		
		{!! Form::label('role', 'Role') !!}

		<select name="role" id="role">
			@foreach($roles as $role)
				@if (Route::current()->getName() == 'users.edit')
					<option value="{{ $role->id }}" {!! (($user->hasRole($role->name)) ? 'selected' : '') !!}>{{ ucfirst($role->name) }}</option>
				@else
					<option value="{{ $role->id }}">{{ ucfirst($role->name) }}</option>
				@endif
			@endforeach
		</select>

	@endif

	{!! Form::submit($buttonText, ['id' => 'submit']) !!}
