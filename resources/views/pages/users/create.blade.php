@extends('layouts.master')

@section('title', 'Create User')

@section('content')
<h1>Create User</h1>


<!-- Form sends user to the courses page -->
<div class="form-group">
	

{!! Form::open(['url' => 'users']) !!}

	@include('pages.users._form', ['buttonText' => 'Create User'])

{!! Form::close() !!}

</div>
<div class="row">
	<a class="btn back" href="{{ url('/users') }}">Back to Users</a>
</div>

@stop