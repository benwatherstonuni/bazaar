@extends('layouts.master')

@section('title', 'Edit - Password')

@section('content')


<h1>Edit Password - {{ $user->first_name . " " . $user->last_name }}</h1>

<!-- Form sends user to the courses page -->
<div class="form-group">
	

{!! Form::open(['method' => 'PATCH', 'action' => ['UsersController@updatePassword', $user->id]]) !!}

	@include('pages.users.edit._changePassword', ['buttonText' => 'Change Password'])

{!! Form::close() !!}

</div>



<div class="row">	

	<a class="btn back" href="{{ url('/users/'. $user->id . '/edit') }}">Back to {{ $user->first_name . " " . $user->last_name }}</a>

</div>

@stop