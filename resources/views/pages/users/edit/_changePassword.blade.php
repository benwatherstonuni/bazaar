
<div class="form-group">
	
	@include('errors.form')

	<!-- If user is logged in hide password update - moved to new page -->
		{!! Form::password('password', ['id' => 'password', 'placeholder'=>'Password']) !!}

		{!! Form::password('password_confirmation', ['id' => 'password_confirmation', 'placeholder'=>'Confirm Password']) !!}		


	{!! Form::submit( $buttonText,['id' => 'submit']) !!}

</div>