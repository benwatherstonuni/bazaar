@extends('layouts.master')

@section('title', 'Edit - ' . $user->first_name . ' ' . $user->last_name )

@section('content')


<h1>Edit - {{ $user->first_name . " " . $user->last_name }}</h1>

<div class="row">
	
	<a class="btn" href="{{ url('/users/'. $user->id . '/edit/password') }}">Change Password</a>
	
</div>

<!-- Form sends user to the courses page -->
<div class="form-group">
	

{!! Form::model($user, ['method' => 'PATCH', 'action' => ['UsersController@update', $user->id]]) !!}

	@include('pages.users._form', ['buttonText' => 'Update User'])

{!! Form::close() !!}

</div>



<div class="row">	

	<a class="btn back" href="{{ url('/users', $user->id) }}">Back to {{ $user->first_name . " " . $user->last_name }}</a>

</div>

@stop