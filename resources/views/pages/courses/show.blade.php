@extends('layouts.master')

@section('title', $course->name)

@section('content')
<h1>{{ $course->name }} - Projects ( {{ count($projects) }} )</h1>


@foreach(array_slice($users->toArray(), 0, 1) as $user)

	@if( auth()->user()->roles->first()->name === "student")

	@else

		<div class="row">

			{!! Form::open(['url' => 'courses/' . $course->id, 'method' => 'DELETE']) !!}
				<button type="submit" class="delete">Delete - {{str_limit($course->name, $limit = 15, $end = '...') }}</button>
			{!! Form::close() !!}
			
			<a class="edit" href="{{ url('/courses/'. $course->id .'/edit') }}">Edit - {{ str_limit($course->name, $limit = 15, $end = '...') }}</a>

			<a class="create" href="{{ url('/projects/create') }}">Add Project</a>
		</div>

	@endif


@endforeach



@if (count($projects) == 0)
	<h2>There are no projects for "{{ $course->name }}" </h2>
@else


@foreach($projects as $project)
<article class="post">

	<a class="btn" href="{{ url('/projects', $project->id) }}">{{ $project->title }}</a>

	<span>{!! str_limit($project->content, $limit = 200, $end = '...') !!}</span>

</article>

@endforeach

@endif

<div class="row">

	<a class="btn back" href="{{ url('/courses/') }}">Back to Courses</a>

</div>

@stop