	{!! Form::label('name', 'Course Name') !!}

	{!! Form::text('name', old('name'), ['id' => 'name', 'placeholder'=>'Course Name']) !!}

	{!! Form::label('user_id', 'Course Leader') !!}

	{!! Form::select('user_id', \App\User::selectArray(), old('user_id'), ['id' => 'user_id']) !!}

	{!! Form::submit( $buttonText, ['class' => 'btn'] ) !!}