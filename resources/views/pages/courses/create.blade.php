@extends('layouts.master')

@section('title', 'Create Course')

@section('content')
<h1>Create Course</h1>


<!-- Form sends user to the courses page -->
<div class="form-group">
@include('errors.form')

{!! Form::open(['url' => 'courses']) !!}

	@include('pages.courses._form', ['buttonText' => 'Create Course'])

{!! Form::close() !!}

</div>

<div class="row">
	<a class="btn back" href="{{ url('/courses/') }}">Back to Courses</a>
</div>
@stop