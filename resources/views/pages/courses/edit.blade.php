@extends('layouts.master')

@section('title', 'Edit - ' . $course->name)

@section('content')
<h1>Edit - {{ $course->name }}</h1>

@include('errors.form')


<!-- Form sends user to the courses page -->
<div class="form-group">
	

{!! Form::model($course, ['method' => 'PATCH', 'action' => ['CoursesController@update', $course->id]]) !!}

	@include('pages.courses._form', ['buttonText' => 'Update Course'])

{!! Form::close() !!}

</div>

<div class="row">

	<a class="btn back" href="{{ url('/courses/' . $course->id) }}">Back to {{ str_limit($course->name, $limit = 15, $end = '...') }}</a>

</div>
@stop
