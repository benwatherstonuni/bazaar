@if ($errors->any())

<!-- Triggers if any errors in form -->
	<ul class="error">
		@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>
@endif