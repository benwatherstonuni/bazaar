<!-- Login Layout -->
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>@yield('title') - Project Bazaar</title>
	<link rel="icon" href="{{ url('images/favicon.ico') }}" type="image/x-icon">

	{!! Html::style('css/style.css') !!}

</head>
<body class="body-login">
	<div class="login-div">
		{{ HTML::image('images/logo.png', 'Project Bazaar Logo', array('class' => 'logo')) }}
		<h1>Project Bazaar @yield('h1')</h1>
		@yield('content')
	</div>

</body>
</html>