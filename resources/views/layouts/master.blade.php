<!-- Master Layout -->
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>@yield('title') - Project Bazaar</title>
	<link rel="icon" href="{{ url('images/favicon.ico') }}" type="image/x-icon">
	{!! Html::style('css/style.css') !!}
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	
	<script src="{{ url('/vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>


</head>
<body>
	@include('inc.header')

	{{-- Flash Message for user feedback --}}
	@if(Session::has('flash_message'))
		
			{!! Session::get('flash_message') !!}
		
	@endif

	<div class="container">


		@yield('content')
	</div>

<script src="https://code.jquery.com/jquery-1.12.2.min.js" integrity="sha256-lZFHibXzMHo3GGeehn1hudTAP3Sc0uKXBXAzHX1sjtk=" crossorigin="anonymous"></script>



<script>
/*
 * Mobile navigation toggle
*/
$(document).ready(function(){
	$('.mobile-menu').click(function(){
		$('nav ul li').toggle();
	});

	$('div.flash span').click(function(){
			$('.flash').slideUp();
	});

});
</script>

</body>
</html>
