<header>
	<div class="logo-container">
		<a href="{{ url('welcome') }}">
			{{ HTML::image('images/logo.png', 'Project Bazaar Logo', array('class' => 'logo')) }}
			<span class="logo-text">Bazaar</span>
		</a>
	</div>
	<nav>
		<ul>
			<li><a href="{{ url('welcome') }}">Home</a></li>
			<li><a href="{{ url('users') }}">Users</a></li>
			<li><a href="{{ url('projects') }}">Projects</a></li>
			<li><a href="{{ url('courses') }}">Courses</a></li>
			<li class="warning"><a href="{{ url('logout') }}">Log out</a></li>
		</ul>
	</nav>
	<a class="mobile-menu" href="#">Menu</a>
</header>