<?php 
$I = new FunctionalTester($scenario);
$I->am('An Admin');
$I->wantTo('Create a new user');

// Log in as Admin
Auth::loginUsingId(1);
$I->seeAuthentication();
// Then check for correct page
$I->amOnPage('/welcome');
// Then go to the users page
$I->click('Users', 'nav a');
// Then
$I->seeInCurrentUrl('/users');
$I->see('Users', 'h1');
// And See a user
$I->see('Create User', 'a');
$I->click('Create User', 'a');
// Then
$I->seeInCurrentUrl('/users/create');
// Then fill out form
$I->submitForm(
    'form',
    [
        'first_name' => 'Joe',
        'last_name' => 'Bloggs',
        'email' => 'joebloggs@email.com',
        'role' => 'Lecturer'
    ],
    'Create User'
);
// Then
$I->amOnPage('/users');
$I->seeRecord('users', array('first_name' => 'Joe', 'last_name' => 'Bloggs', 'email' => 'joebloggs@email.com'));

