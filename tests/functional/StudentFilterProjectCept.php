<?php 
$I = new FunctionalTester($scenario);
$I->am('A Student');
$I->wantTo('Filter project');

// Log in as Admin
Auth::loginUsingId(12);
$I->seeAuthentication();
// Then check for correct page
$I->amOnPage('/welcome');
// Then check for correct User Name and content
$I->see('Student!');
$I->see('Welcome to Project Bazaar');
// Then go to the users page
$I->click('Courses', 'nav a');
// Then
$I->see('Courses', 'h1');
// Then
$I->see('Computing', 'ul.list li a');
$I->click('Computing', 'ul.list li a');
// Then choose prject from filtered list
$I->see('Project 17', 'a');
$I->click('Project 17', 'a');
// Then
$I->see('Project 17', 'h1');