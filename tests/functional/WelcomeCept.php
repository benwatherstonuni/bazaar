<?php 
$I = new FunctionalTester($scenario);
$I->am('An Admin');
$I->wantTo('To log in and see the welcome page');

// Log in as Admin
Auth::loginUsingId(1);
$I->seeAuthentication();
// Then check for correct page
$I->amOnPage('/welcome');
// Then check for correct User Name and content
$I->see('Ben!');
$I->see('Welcome to Project Bazaar');