<?php 
$I = new FunctionalTester($scenario);
$I->am('A Lecturer');
$I->wantTo('Add project');

// Log in as Admin
Auth::loginUsingId(2);
$I->seeAuthentication();
// Then check for correct page
$I->amOnPage('/welcome');
// Then check for correct User Name and content
$I->see('Dave!');
$I->see('Welcome to Project Bazaar');
// Then go to the users page
$I->click('Projects', 'nav a');
// Then
$I->see('Most Recent Projects', 'h1');
// Then
$I->see('Create Project', 'a');
$I->click('Create Project', 'a');
// Then
$I->see('Create Project', 'h1');

$I->fillField('title', 'test project');
$I->selectOption('form select[name=leader_id]', 'Dave Walsh');
$I->selectOption('form select[name=course_id]', 'Computing');
$I->fillField('content', 'test content');
// Then
$I->click('Create Project', 'input.btn');
// Then
$I->amOnPage('/projects');
$I->see('test project', 'a.btn');
$I->see('test content', '');