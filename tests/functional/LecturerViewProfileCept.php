<?php 
$I = new FunctionalTester($scenario);
$I->am('A Lecturer');
$I->wantTo('View profile');

// Log in as Admin
Auth::loginUsingId(2);
$I->seeAuthentication();
// Then check for correct page
$I->amOnPage('/welcome');
// Then check for correct User Name and content
$I->see('Dave!');
$I->see('Welcome to Project Bazaar');
// Then go to the users page
$I->click('Users', 'nav a');
// Then
$I->see('Users', 'h1');
// Then
$I->see('My Profile', 'a');
$I->click('My Profile', 'a');
// Then
$I->see('User Profile', 'h1');
$I->see('Dave Walsh', 'p.user');