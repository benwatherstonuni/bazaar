<?php 
$I = new FunctionalTester($scenario);
$I->am('An Admin');
$I->wantTo('Go to the Users page and see a list a Users');

// Log in as Admin
Auth::loginUsingId(1);
$I->seeAuthentication();
// Then check for correct page
$I->amOnPage('/welcome');
// Then go to the users page
$I->click('Users', 'nav a');
// Then
$I->see('Users', 'h1');
// And See a user
$I->see('Dave Walsh', 'td');
$I->see('walshd@edgehill.ac.uk', 'td');
$I->see('Lecturer', 'td');
// Then check to see if another user is there
$I->see('Mark Hall', 'td');
$I->see('mark.hall@edgehill.ac.uk', 'td');
$I->see('Lecturer', 'td');
