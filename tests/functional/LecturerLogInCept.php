<?php 
$I = new FunctionalTester($scenario);
$I->am('A Lecturer');
$I->wantTo('To log in and see the welcome page');

// Log in as Admin
Auth::loginUsingId(2);
$I->seeAuthentication();
// Then check for correct page
$I->amOnPage('/welcome');
// Then check for correct User Name and content
$I->see('Dave!');
$I->see('Welcome to Project Bazaar');