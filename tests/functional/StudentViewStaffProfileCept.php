<?php 
$I = new FunctionalTester($scenario);
$I->am('A Student');
$I->wantTo('View Staff Profile');

// Log in as Admin
Auth::loginUsingId(12);
$I->seeAuthentication();
// Then check for correct page
$I->amOnPage('/welcome');
// Then check for correct User Name and content
$I->see('Student!');
$I->see('Welcome to Project Bazaar');
// Then go to the users page
$I->click('Users', 'nav a');
// Then
$I->see('Users', 'h1');
// Then
$I->see('View', 'a');
$I->click('View', 'a');
// Then
$I->see('User Profile', 'h1');
$I->see('Mark Anderson', 'p.user');