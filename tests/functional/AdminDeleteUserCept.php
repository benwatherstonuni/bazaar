<?php 
$I = new FunctionalTester($scenario);
$I->am('An Admin');
$I->wantTo('Delete a user');

// Log in as Admin
Auth::loginUsingId(1);
$I->seeAuthentication();
// Then check for correct page
$I->amOnPage('/welcome');
// Then go to the users page
$I->click('Users', 'nav a');
// Then
$I->seeInCurrentUrl('/users');
$I->see('Users', 'h1');
// And See a user
$I->see('Mark Anderson', 'td');
$I->see('View', 'a.view');
$I->click('View', 'a.view');
// Then
$I->see('User Profile', 'h1');
$I->see('Mark Anderson', 'p.user');
$I->see('Delete', 'button');
$I->click('Delete', 'button');
// Then
$I->seeInCurrentUrl('/users');
$I->dontSee('Mark Anderson', 'td');