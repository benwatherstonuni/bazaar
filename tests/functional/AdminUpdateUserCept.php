<?php 
$I = new FunctionalTester($scenario);
$I->am('An Admin');
$I->wantTo('Update a new user');

// Log in as Admin
Auth::loginUsingId(1);
$I->seeAuthentication();
// Then check for correct page
$I->amOnPage('/welcome');
// Then go to the users page
$I->click('Users', 'nav a');
// Then
$I->seeInCurrentUrl('/users');
$I->see('Users', 'h1');
// And See a user
$I->see('View', 'a.view');
$I->click('View', 'a.view');
// Then
$I->see('User Profile', 'h1');
$I->see('Edit - Mark Anderson', 'a');
$I->click('Edit - Mark Anderson', 'a');
// Then
$I->see('Edit - Mark Anderson', 'h1');
$I->fillField('first_name', 'Marky');
// Then
$I->click('Update User', '#submit');
$I->see('Edit - Marky Anderson', 'h1');


