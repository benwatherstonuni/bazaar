<?php 
$I = new FunctionalTester($scenario);
$I->am('A Student');
$I->wantTo('View Project List');

// Log in as Admin
Auth::loginUsingId(12);
$I->seeAuthentication();
// Then check for correct page
$I->amOnPage('/welcome');
// Then check for correct User Name and content
$I->see('Student!');
$I->see('Welcome to Project Bazaar');
// Then go to the users page
$I->click('Projects', 'nav a');
// Then
$I->see('Most Recent Projects', 'h1');
// Then
$I->see('project 1', 'a');
$I->see('project 2', 'a');
$I->see('project 3', 'a');
