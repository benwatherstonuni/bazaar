<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker; 

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Clear the table of data
         */
        DB::table('projects')->truncate();

        /**
         * Create an array containing all project names
         */
        $projects = [
            'Project 1',
            'Project 2',
            'Project 3',
            'Project 4',
            'Project 5',
            'Project 6',
            'Project 7',
            'Project 8',
            'Project 9',
            'Project 10',
            'Project 11',
            'Project 12',
            'Project 13',
            'Project 14',
            'Project 15',
            'Project 16',
            'Project 17',
            'Project 18',
            'Project 19',
            'Project 20',
            'Project 21',
            'Project 22',
            'Project 23',
            'Project 24',
            'Project 25'
        ];

        $faker = Faker::create();

        /**
         * Loop though projects and create a project with random data for each project
         */
        foreach($projects as $key => $projectName) {
            /**
             * Add database entry for new project assigning project name, random project code and rproject leader
             */
            DB::table('projects')->insert([
                'title'     => $projectName,
                'leader_id' => rand(1, 2),
                'course_id' => rand(1, 8),
                'content'   => $faker->realText(1000, 3)
            ]);

        }

    }
}
