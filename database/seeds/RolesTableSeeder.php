<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         /**
         * Clear the table of data
         */
        DB::table('roles')->truncate();

        /**
         * Create an array containing all role names
         */
        $roles = [
            'admin',
            'lecturer',
            'student',
        ];

        /**
         * Loop though roles and create a role with random data for each course
         */
        foreach($roles as $key => $roleName) {
            /**
             * Add database entry for new role assigning role name
             */
            DB::table('roles')->insert([
                'name' => $roleName
            ]);

        }
    }
}
