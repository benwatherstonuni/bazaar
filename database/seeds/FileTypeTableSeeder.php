<?php

use Illuminate\Database\Seeder;

class FileTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Clear the table of data
         */
        DB::table('file_types')->truncate();

        /**
         * Create an array containing all course names
         */
        $fileType = [
            '.doc',
            '.pdf',
            '.jpg',
            '.png',
        ];

        /**
         * Loop though courses and create a course with random data for each course
         */
        foreach($fileType as $key => $fileTypeName) {
            /**
             * Add database entry for new course assigning course name, random course code and random course leader
             */
            DB::table('file_types')->insert([
                'name' => $fileTypeName
            ]);

        }
    }
}
