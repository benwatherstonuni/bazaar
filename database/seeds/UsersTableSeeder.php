<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       /**
         * Clear the table of data
         */
        // DB::table('users')->truncate();

        /**
         * Create an array containing all Users first names
         */
        $first_names = [
            'Ben',
            'Dave',
            'Mark',
            'Daniel',
            'Mark',
            'Ardhendu',
            'Susan',
           	'James',
           	'Collette',
           	'Hui'
       
        ];

        /**
         * Create an array containing all users last names
         */
        $last_names = [
            'Watherston',
            'Walsh',
            'Hall',
            'Reil',
            'Anderson',
            'Behera',
            'Canning',
            'Coleman',
            'Gavan',
            'Fang'
       
        ];

        /**
         * Create an array containing all users last names
         */
        $email = [
            '22028048@go.edgehill.ac.uk',
            'walshd@edgehill.ac.uk',
            'mark.hall@edgehill.ac.uk',
            'daniel.reil@edgehill.ac.uk',
            'mark.anderson@edgehill.ac.uk',
            'behera@edgehill.ac.uk',
            'cannings@edgehill.ac.uk',
            'colemanj@edgehill.ac.uk',
            'gavanc@edgehill.ac.uk',
            'hui.fang@edgehill.ac.uk'
        ];


        /**
         * Loop though users and create a project with random data for each project
         */
        foreach($email as $key => $email) {
            /**
             * Add database entry for new project assigning project name, random project code and rproject leader
             */
            $user = User::create([
                'first_name'  => $first_names[$key],
                'last_name' => $last_names[$key],
                'email' => $email,
                'password' => bcrypt('password')
            ]);

            if ($first_names[$key] == 'Ben')
            {
                $user->roles()->attach(1);
            }
            else
            {
                $user->roles()->attach(3);
            }

        }
    }
}
