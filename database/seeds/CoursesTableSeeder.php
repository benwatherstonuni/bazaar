<?php

use Illuminate\Database\Seeder;

class CoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Clear the table of data
         */
        DB::table('courses')->truncate();

        /**
         * Create an array containing all course names
         */
        $courses = [
            'Computing',
            'Application Development',
            'Games Programming',
            'Information Systems',
            'Networking, Security and Forensics',
            'Systems and Software',
            'Information Technology Management for Business',
            'Web Design and Development',
        ];

        /**
         * Loop though courses and create a course with random data for each course
         */
        foreach($courses as $key => $courseName) {
            /**
             * Add database entry for new course assigning course name, random course code and random course leader
             */
            DB::table('courses')->insert([
                'name' => $courseName
            ]);

        }

    }
}
