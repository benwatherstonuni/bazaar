<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
        $permissions = [
        	'manage-projects',
        	'manage-courses',
        	'manage-users'
        ];

        /**
         * Loop though roles and create a role with random data for each course
         */
        foreach($permissions as $permission) {
            /**
             * Add database entry for new role assigning role name
             */
            DB::table('permissions')->insert([
                'name' => $permission
            ]);

        }
    }
}
